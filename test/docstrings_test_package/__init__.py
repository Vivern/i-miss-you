"""
<docstrings_test_package summary>

<docstrings_test_package details>
"""

from collections import defaultdict as defaultdict

from . import public_submodule as public_submodule
from .internal_module import *


def helper_function():
    """
    ## Metadata

    `public`: False
    """
